﻿namespace Server.Core.Schema
{
    public interface IAppLoader
    {
        IApp LoadApp(string location, LoadRunMode mode);

        IApp LoadApp(string corLocation, string appLocation, LoadRunMode mode);
    };
}