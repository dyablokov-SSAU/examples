﻿using System;

namespace Server.Core.Schema
{
    public interface IApp
    {
        void Close();

        int ProcessId { get; }
        string Location { get; }
        string Name { get; }
        Version Version { get; }
        string Edition { get; }
        string Info { get; }
        Version EngineVersion { get; }
        string SchemaSignature { get; }
    };
}