﻿using System;
using System.Runtime.Serialization;

namespace Server.Core.Exceptions
{
    public class DbIntegrityViolationException : Exception
    {
        protected DbIntegrityViolationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public DbIntegrityViolationException() : base()
        {
        }

        public DbIntegrityViolationException(string message) : base(message)
        {
        }

        public DbIntegrityViolationException(string message, Exception innerException) : base(message, innerException)
        {
        }
    };
}