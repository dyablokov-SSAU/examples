﻿using System;
using System.Runtime.Serialization;

namespace Server.Core.Exceptions
{
    public class DiagramIntegrityViolationException : Exception
    {
        protected DiagramIntegrityViolationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public DiagramIntegrityViolationException() : base()
        {
        }

        public DiagramIntegrityViolationException(string message) : base(message)
        {
        }

        public DiagramIntegrityViolationException(string message, Exception innerException) : base(message, innerException)
        {
        }
    };
}
