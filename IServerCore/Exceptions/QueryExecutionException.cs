﻿using System;
using System.Runtime.Serialization;

namespace Server.Core.Exceptions
{
    public class QueryExecutionException : Exception
    {
        protected QueryExecutionException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public QueryExecutionException() : base()
        {
        }

        public QueryExecutionException(string message) : base(message)
        {
        }

        public QueryExecutionException(string message, Exception innerException) : base(message, innerException)
        {
        }
    };
}
