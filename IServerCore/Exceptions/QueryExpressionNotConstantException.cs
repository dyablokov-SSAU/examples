﻿using System;
using System.Runtime.Serialization;

namespace Server.Core.Exceptions
{
    public class QueryExpressionNotConstantException : Exception
    {
        protected QueryExpressionNotConstantException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public QueryExpressionNotConstantException() : base()
        {
        }

        public QueryExpressionNotConstantException(string message) : base(message)
        {
        }

        public QueryExpressionNotConstantException(string message, Exception innerException) : base(message, innerException)
        {
        }
    };
}
