﻿using System;
using System.Runtime.Serialization;

namespace Server.Core.Exceptions
{
    public class DbObjectNameNotUniqueException : Exception
    {
        protected DbObjectNameNotUniqueException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public DbObjectNameNotUniqueException() : base()
        {
        }

        public DbObjectNameNotUniqueException(string message) : base(message)
        {
        }

        public DbObjectNameNotUniqueException(string message, Exception innerException) : base(message, innerException)
        {
        }
    };
}