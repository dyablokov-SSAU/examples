﻿using System;
using System.Runtime.Serialization;

namespace Server.Core.Exceptions
{
    public class ItemNotFoundException : Exception
    {
        protected ItemNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public ItemNotFoundException() : base()
        {
        }

        public ItemNotFoundException(string message) : base(message)
        {
        }

        public ItemNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }
    };
}
