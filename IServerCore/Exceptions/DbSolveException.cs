﻿using System;
using System.Runtime.Serialization;

namespace Server.Core.Exceptions
{
    public class DbSolveException : Exception
    {
        protected DbSolveException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public DbSolveException() : base()
        {
        }

        public DbSolveException(string message) : base(message)
        {
        }

        public DbSolveException(string message, Exception innerException) : base(message, innerException)
        {
        }
    };
}
