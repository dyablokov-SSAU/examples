﻿using System;
using System.Runtime.Serialization;

namespace Server.Core.Exceptions
{
    public class InvalidHandleException : Exception
    {
        protected InvalidHandleException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public InvalidHandleException() : base()
        {
        }

        public InvalidHandleException(string message) : base(message)
        {
        }

        public InvalidHandleException(string message, Exception innerException) : base(message, innerException)
        {
        }
    };
}