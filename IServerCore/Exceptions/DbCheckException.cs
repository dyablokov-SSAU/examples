﻿using System;
using System.Runtime.Serialization;

namespace Server.Core.Exceptions
{
    public class DbCheckException : Exception
    {
        protected DbCheckException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public DbCheckException() : base()
        {
        }

        public DbCheckException(string message) : base(message)
        {
        }

        public DbCheckException(string message, Exception innerException) : base(message, innerException)
        {
        }
    };
}
