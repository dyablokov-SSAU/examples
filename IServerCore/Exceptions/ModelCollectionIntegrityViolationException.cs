﻿using System;
using System.Runtime.Serialization;

namespace Server.Core.Exceptions
{
    public class ModelCollectionIntegrityViolationException : Exception
    {
        protected ModelCollectionIntegrityViolationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public ModelCollectionIntegrityViolationException() : base()
        {
        }

        public ModelCollectionIntegrityViolationException(string message) : base(message)
        {
        }

        public ModelCollectionIntegrityViolationException(string message, Exception innerException) : base(message, innerException)
        {
        }
    };
}