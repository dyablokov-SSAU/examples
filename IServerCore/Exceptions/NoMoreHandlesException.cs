﻿using System;
using System.Runtime.Serialization;

namespace Server.Core.Exceptions
{
    public class NoMoreHandlesException : Exception
    {
        protected NoMoreHandlesException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public NoMoreHandlesException() : base()
        {
        }

        public NoMoreHandlesException(string message) : base(message)
        {
        }

        public NoMoreHandlesException(string message, Exception innerException) : base(message, innerException)
        {
        }
    };
}