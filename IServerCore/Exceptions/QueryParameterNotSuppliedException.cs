﻿using System;
using System.Runtime.Serialization;

namespace Server.Core.Exceptions
{
    public class QueryParameterNotSuppliedException : Exception
    {
        protected QueryParameterNotSuppliedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public QueryParameterNotSuppliedException()
        {
        }

        public QueryParameterNotSuppliedException(string message) : base(message)
        {
        }

        public QueryParameterNotSuppliedException(string message, Exception innerException) : base(message, innerException)
        {
        }
    };
}