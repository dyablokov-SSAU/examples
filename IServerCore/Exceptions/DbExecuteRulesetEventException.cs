﻿using System;
using System.Runtime.Serialization;

namespace Server.Core.Exceptions
{
    public class DbExecuteRulesetEventException : Exception
    {
        protected DbExecuteRulesetEventException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public DbExecuteRulesetEventException() : base()
        {
        }

        public DbExecuteRulesetEventException(string message) : base(message)
        {
        }

        public DbExecuteRulesetEventException(string message, Exception innerException) : base(message, innerException)
        {
        }
    };
}