﻿using System;
using System.Runtime.Serialization;

namespace Server.Core.Exceptions
{
    public class InvalidNativeEntryPointException : Exception
    {
        protected InvalidNativeEntryPointException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public InvalidNativeEntryPointException() : base()
        {
        }

        public InvalidNativeEntryPointException(string message) : base(message)
        {
        }

        public InvalidNativeEntryPointException(string message, Exception innerException) : base(message, innerException)
        {
        }
    };
}