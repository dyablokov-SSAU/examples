﻿using System;
using System.Runtime.Serialization;

namespace Server.Core.Exceptions
{
    public class InvalidApplicationException : Exception
    {
        protected InvalidApplicationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public InvalidApplicationException() : base()
        {
        }

        public InvalidApplicationException(string message) : base(message)
        {
        }

        public InvalidApplicationException(string message, Exception innerException) : base(message, innerException)
        {
        }
    };
}