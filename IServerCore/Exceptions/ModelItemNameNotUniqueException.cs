﻿using System;
using System.Runtime.Serialization;

namespace Server.Core.Exceptions
{
    public class ModelItemNameNotUniqueException : Exception
    {
        protected ModelItemNameNotUniqueException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public ModelItemNameNotUniqueException() : base()
        {
        }

        public ModelItemNameNotUniqueException(string message) : base(message)
        {
        }

        public ModelItemNameNotUniqueException(string message, Exception innerException) : base(message, innerException)
        {
        }
    };
}  