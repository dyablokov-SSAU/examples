﻿using System;
using System.Runtime.Serialization;

namespace Server.Core.Exceptions
{
    public class AttemptToExecuteFromWrongThreadException : Exception
    {
        protected AttemptToExecuteFromWrongThreadException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public AttemptToExecuteFromWrongThreadException() : base()
        {
        }

        public AttemptToExecuteFromWrongThreadException(string message) : base(message)
        {
        }

        public AttemptToExecuteFromWrongThreadException(string message, Exception innerException) : base(message, innerException)
        {
        }
    };
}
