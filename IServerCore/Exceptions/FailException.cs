﻿using System;
using System.Runtime.Serialization;

namespace Server.Core.Exceptions
{
    public class FailException : Exception
    {
        protected FailException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public FailException() : base()
        {
        }

        public FailException(string message) : base(message)
        {
        }

        public FailException(string message, Exception innerException) : base(message, innerException)
        {
        }
    };
}