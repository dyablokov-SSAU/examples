﻿using System;
using System.Runtime.Serialization;

namespace Server.Core.Exceptions
{
    public class UnspecifiedObjectNameException : Exception
    {
        protected UnspecifiedObjectNameException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public UnspecifiedObjectNameException() : base()
        {
        }

        public UnspecifiedObjectNameException(string message) : base(message)
        {
        }

        public UnspecifiedObjectNameException(string message, Exception innerException) : base(message, innerException)
        {
        }
    };
}
