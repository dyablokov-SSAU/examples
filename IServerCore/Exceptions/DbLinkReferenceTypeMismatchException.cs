﻿using System;
using System.Runtime.Serialization;

namespace Server.Core.Exceptions
{
    public class DbLinkReferenceTypeMismatchException : Exception
    {
        protected DbLinkReferenceTypeMismatchException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public DbLinkReferenceTypeMismatchException() : base()
        {
        }

        public DbLinkReferenceTypeMismatchException(string message) : base(message)
        {
        }

        public DbLinkReferenceTypeMismatchException(string message, Exception innerException) : base(message, innerException)
        {
        }
    };
}