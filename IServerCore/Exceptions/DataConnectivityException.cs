﻿using System;
using System.Runtime.Serialization;

namespace Server.Core.Exceptions
{
    public class DataConnectivityException : Exception
    {
        protected DataConnectivityException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public DataConnectivityException() : base()
        {
        }

        public DataConnectivityException(string message) : base(message)
        {
        }

        public DataConnectivityException(string message, Exception innerException) : base(message, innerException)
        {
        }
    };
}