﻿using System;
using System.Runtime.Serialization;

namespace Server.Core.Exceptions
{
    public class ApplicationInitException : Exception
    {
        protected ApplicationInitException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public ApplicationInitException() : base()
        {
        }

        public ApplicationInitException(string message) : base(message)
        {
        }

        public ApplicationInitException(string message, Exception innerException) : base(message, innerException)
        {
        }
    };
}