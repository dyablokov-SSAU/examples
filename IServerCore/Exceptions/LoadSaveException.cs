﻿using System;
using System.Runtime.Serialization;

namespace Server.Core.Exceptions
{
    public class LoadSaveException : Exception
    {
        protected LoadSaveException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public LoadSaveException() : base()
        {
        }

        public LoadSaveException(string message) : base(message)
        {
        }

        public LoadSaveException(string message, Exception innerException) : base(message, innerException)
        {
        }
    };
}