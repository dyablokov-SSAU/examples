﻿using System;
using System.Runtime.Serialization;

namespace Server.Core.Exceptions
{
    public class NullOrEmptyException : Exception
    {
        protected NullOrEmptyException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public NullOrEmptyException() : base()
        {
        }

        public NullOrEmptyException(string message) : base(message)
        {
        }

        public NullOrEmptyException(string message, Exception innerException) : base(message, innerException)
        {
        }
    };
}