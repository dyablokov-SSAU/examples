﻿using System;
using System.Runtime.Serialization;

namespace Server.Core.Exceptions
{
    public class SqlParseException : Exception
    {
        protected SqlParseException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public SqlParseException() : base()
        {
        }

        public SqlParseException(string message) : base(message)
        {
        }

        public SqlParseException(string message, Exception innerException) : base(message, innerException)
        {
        }
    };
}
