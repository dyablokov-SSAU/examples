﻿using System;
using System.Runtime.Serialization;

namespace Server.Core.Exceptions
{
    public class ApplicationLoadException : Exception
    {
        protected ApplicationLoadException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public ApplicationLoadException() : base()
        {
        }

        public ApplicationLoadException(string message) : base(message)
        {
        }

        public ApplicationLoadException(string message, Exception innerException) : base(message, innerException)
        {
        }
    };
}