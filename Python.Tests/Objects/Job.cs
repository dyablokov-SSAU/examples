﻿using INFOPRO.Examples.Python.Constants;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace INFOPRO.Examples.Python.Tests.Objects
{
    internal class Job : IJob
    {
        private Dictionary<Guid, JobMessage> _messages;
        private Dictionary<string, string> _variables;
        
        public Job(Guid id)
        {
            _messages = new Dictionary<Guid, JobMessage>();
            _variables = new Dictionary<string, string>();
            Id = id;
        }

        public Guid Id { get; }

        public void CreateMessage(JobMessageKind messageType, JobMessageSource messageSource, string text)
        {
            var message = new JobMessage(messageType, messageSource, text);
            _messages.Add(Guid.NewGuid(), message);
        }

        public async Task CreateMessageAsync(JobMessageKind messageType, JobMessageSource messageSource, string message)
        {
            await Task.Run(() => CreateMessage(messageType, messageSource, message));
        }

        public IDictionary<Guid, JobMessage> GetMessages() => new Dictionary<Guid, JobMessage>(_messages);

        public async Task<IDictionary<Guid, JobMessage>> GetMessagesAsync()
        {
            return await Task.Run(GetMessages);
        }

        public void CreateOrUpdateVariable(string name, string value)
        {
            if (!_variables.ContainsKey(name))
            {
                _variables.Add(name, value);
                return;
            }

            _variables[name] = value;
        }

        public async Task CreateOrUpdateVariableAsync(string name, string value)
        {
            await Task.Run(() => CreateOrUpdateVariable(name, value));
        }

        public IEnumerable<string> GetVariableNames() => _variables.Keys;

        public async Task<IEnumerable<string>> GetVariableNamesAsync()
        {
            return await Task.Run(GetVariableNames);
        }

        public string GetVariableValue(string name)
        {
            if (!_variables.TryGetValue(name, out var result))
                throw new ArgumentOutOfRangeException(nameof(name));

            return result;
        }

        public async Task<string> GetVariableValueAsync(string name)
        {
            return await Task.Run(() => GetVariableValue(name));
        }
    };

}
