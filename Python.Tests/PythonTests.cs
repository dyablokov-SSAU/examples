using INFOPRO.Examples.Python.Constants;
using INFOPRO.Examples.Python.Tests.Fixtures;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Xunit;

namespace INFOPRO.Examples.Python.Tests
{
    [Trait("Python", "Unit")]
    public class PythonTests : IClassFixture<PythonFixture>
    {
        private PythonFixture _fixture;

        public PythonTests(PythonFixture fixture)
        {
            _fixture = fixture;
        }

        public static TheoryData<Guid, Dictionary<string, string>> GetIronPythonArgs()
        {
            var data = new TheoryData<Guid, Dictionary<string, string>>();
            
            var variables = new Dictionary<string, string>
            {
                ["var1"] = "10",
                ["var2"] = "20"
            };

            data.Add(Guid.NewGuid(), variables);

            return data;
        }

        [Theory]
        [MemberData(nameof(GetIronPythonArgs))]
        public void IronPython_Execute_Success(Guid id, IEnumerable<KeyValuePair<string, string>> variables)
        {
            // Arrange
            var job = _fixture.CreateJob(id, variables.ToArray());

            var script = _fixture.CreateScriptParam
            (
                $"print('{ScriptShell.IronPython}')",
                "var3 = ';'.join([str(i) for i in range(int(var1), int(var2), 2)])"
            );
            
            var inptVarsParam = _fixture.CreateInputVarsParam("var1", "var2");
            var outputVarsParam = _fixture.CreateOutputVarsParam("var3");
            var parameters = _fixture.CreateIronPythonParams(script, inptVarsParam, outputVarsParam);
            
            // Act
            using (var executor = new PythonExecutor())
            {
                executor.Initialize(job, parameters);
                executor.Execute();
            }

            var messages = job.GetMessages();
            var varNames = job.GetVariableNames().ToArray();
            
            // Assert
            Assert.True(messages.Count == 1);
            Assert.Equal($"{ScriptShell.IronPython}", messages.First().Value.Text);
            Assert.True(varNames.Length == 3);
            
            var var3Value = job.GetVariableValue("var3");
            Assert.Contains("16", var3Value);
            Assert.True(var3Value.Split(";".ToCharArray()).Length == 5);
        }

        [Fact]
        public void LocalPython_Execute_Success()
        {
            // Arrange
            var job = _fixture.CreateJob
            (
                Guid.NewGuid(),
                _fixture.MakePair("var1", "8"),
                _fixture.MakePair("var2", "6")
            );

            var script = _fixture.CreateScriptParam
            (
                $"print('{ScriptShell.LocalPython}')",
                "var3 = 2 + float(var1)",
                "var2 = var2**2",
                "var4 = [i for i in range(var2)]"
            );

            var inptVarsParam = _fixture.CreateInputVarsParam("var1", "var2");
            var outputVarsParam  = _fixture.CreateOutputVarsParam("var2", "var3", "var4");
            var parameters = _fixture.CreateLocalPythonParams(script, inptVarsParam, outputVarsParam, null, @"C:\ProgramData\Anaconda3\python.exe");

            // Act
            using (var executor = new PythonExecutor())
            {
                executor.Initialize(job, parameters);
                executor.Execute();
            }

            var messages = job.GetMessages();
            var varNames = job.GetVariableNames().ToArray();

            // Assert
            Assert.True(messages.Count == 1);
            Assert.Equal($"{ScriptShell.LocalPython}", messages.First().Value.Text);
            Assert.True(varNames.Length == 4);
            Assert.Equal(8, Convert.ToInt32(job.GetVariableValue("var1")));
            Assert.Equal(36, Convert.ToInt32(job.GetVariableValue("var2")));
            Assert.Equal(10.0, Convert.ToDouble(job.GetVariableValue("var3"), CultureInfo.InvariantCulture));
            Assert.True(job.GetVariableValue("var4").Length == 135);

        }
    };
}
