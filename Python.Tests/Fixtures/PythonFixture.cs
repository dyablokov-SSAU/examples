﻿using INFOPRO.Examples.Python.Constants;
using INFOPRO.Examples.Python.Tests.Objects;
using INFOPRO.Examples.Python.Xml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace INFOPRO.Examples.Python.Tests.Fixtures
{
    public class PythonFixture
    {
        public IJob CreateJob(Guid id, params KeyValuePair<string, string>[] nameValues)
        {
            var result = new Job(id);
            
            foreach (var pair in nameValues)
                result.CreateOrUpdateVariable(pair.Key, pair.Value);

            return result;
        }

        public string CreateScriptParam(params string[] lines)
        {
            var builder = new StringBuilder();

            foreach (var line in lines)
                builder.AppendLine(line);

            return builder.ToString();
        }

        public string CreateInputVarsParam(params string[] varNames)
        {
            return string.Join(',', varNames);
        }

        public string CreateOutputVarsParam(params string[] varNames)
        {
            return string.Join(',', varNames);
        }

        public string CreateSearchPathsParam(params string[] searchPaths)
        {
            return string.Join(';', searchPaths);
        }

        public string CreateIronPythonParams(string script, string inputVars, string outputVars, string searchPaths = null)
        {
            var xmlParams = new XmlParameterCollection();

            var engineParam = new XmlParameter(Enum.GetName(typeof(ScriptShell), ScriptShell.IronPython));
            engineParam.Attributes.Add("Name", XmlParameterTraits.Names.Engine);
            xmlParams.Add(engineParam);

            var scriptParam = new XmlParameter(script);
            scriptParam.Attributes.Add("Name", XmlParameterTraits.Names.Script);
            xmlParams.Add(scriptParam);

            var inputVarsParam = new XmlParameter(inputVars);
            inputVarsParam.Attributes.Add("Name", XmlParameterTraits.Names.InputVariables);
            xmlParams.Add(inputVarsParam);

            var outputVarsParam = new XmlParameter(outputVars);
            outputVarsParam.Attributes.Add("Name", XmlParameterTraits.Names.OutputVariables);
            xmlParams.Add(outputVarsParam);

            var searchPathsParam = new XmlParameter(searchPaths);
            searchPathsParam.Attributes.Add("Name", XmlParameterTraits.Names.SearchPaths);
            xmlParams.Add(searchPathsParam);

            XmlHelper.SerializeToString(xmlParams, out var result);

            return result;
        }

        public string CreateLocalPythonParams(string script, string inputVars, string outputVars, string initCommands, string engineFile)
        {
            var xmlParams = new XmlParameterCollection();

            var engineParam = new XmlParameter(Enum.GetName(typeof(ScriptShell), ScriptShell.LocalPython));
            engineParam.Attributes.Add("Name", XmlParameterTraits.Names.Engine);
            xmlParams.Add(engineParam);

            var scriptParam = new XmlParameter(script);
            scriptParam.Attributes.Add("Name", XmlParameterTraits.Names.Script);
            xmlParams.Add(scriptParam);

            var inputVarsParam = new XmlParameter(inputVars);
            inputVarsParam.Attributes.Add("Name", XmlParameterTraits.Names.InputVariables);
            xmlParams.Add(inputVarsParam);

            var outputVarsParam = new XmlParameter(outputVars);
            outputVarsParam.Attributes.Add("Name", XmlParameterTraits.Names.OutputVariables);
            xmlParams.Add(outputVarsParam);

            var initCommandsParam = new XmlParameter(initCommands);
            initCommandsParam.Attributes.Add("Name", XmlParameterTraits.Names.InitCommands);
            xmlParams.Add(initCommandsParam);

            var engineFileParam = new XmlParameter(engineFile);
            engineFileParam.Attributes.Add("Name", XmlParameterTraits.Names.EngineFile);
            xmlParams.Add(engineFileParam);

            XmlHelper.SerializeToString(xmlParams, out var result);

            return result;
        }

        public KeyValuePair<TKey, TValue> MakePair<TKey, TValue>(TKey key, TValue value)
        {
            return new KeyValuePair<TKey, TValue>(key, value);
        }
    };
}
