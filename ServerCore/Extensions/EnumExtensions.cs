﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Server.Core.Extensions
{
    public static class EnumExtensions
    {
        public static T ToEnum<T>(this int value) where T : Enum
        {
            if (!Enum.IsDefined(typeof(T), value))
                throw new ArgumentOutOfRangeException(nameof(value));

            return (T)(object)value;
        }
    }
}
