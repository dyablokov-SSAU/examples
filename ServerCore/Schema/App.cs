﻿using Server.Core.NativeAPI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Server.Core.Schema
{
    internal class App : IApp
    {
        public App(int hApp, int processId)
        {
            HApp = hApp;
            ProcessId = processId;
        }

        public virtual void Close() => Native.CloseApplication(HApp);

        public virtual int HApp { get; }
        
        public virtual int ProcessId { get; }

        public virtual string Location => Native.GetApplicationLocation(HApp);

        public virtual string Name => Native.GetApplicationName(HApp);

        public virtual Version Version
        {
            get
            {
                var result = Native.GetApplicationVersion(HApp);
                return Version.Parse(result);
            }
        }

        public virtual string Edition => Native.GetApplicationEdition(HApp);
        
        public virtual string Info => Native.GetApplicationInfo(HApp);

        public virtual Version EngineVersion
        {
            get
            {
                var result = Native.GetEngineVersion(HApp);
                return Version.Parse(result);
            }
        }

        public virtual string SchemaSignature => Native.GetSchemaSignature(HApp);



    };
}