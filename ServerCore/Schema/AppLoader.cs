﻿using Server.Core.Exceptions;
using Server.Core.NativeAPI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Server.Core.Schema
{
    public class AppLoader : IAppLoader
    {
        protected readonly Dictionary<string, IntPtr> _corLocations = new Dictionary<string, IntPtr>();

        protected virtual void AddCorLocation(string corLocation)
        {
            if (string.IsNullOrEmpty(corLocation))
                throw new NullOrEmptyException("Loader location is null or empty.");

            if (!Directory.Exists(corLocation))
                throw new DirectoryNotFoundException($"Loader location '{corLocation}' not found.");

            if (_corLocations.ContainsKey(corLocation))
                return;

            var cookie = Native.AddDllDirectory(corLocation);

            if (IntPtr.Zero != cookie)
                _corLocations.Add(corLocation, cookie);
        }

        public virtual void Dispose()
        {
            foreach (var pair in _corLocations)
                Native.RemoveDllDirectory(pair.Value);
        }

        public virtual IApp LoadApp(string location, LoadRunMode mode)
        {
            if (string.IsNullOrEmpty(location))
                throw new NullOrEmptyException("Application location is null or empty.");

            if (!Directory.Exists(location))
                throw new DirectoryNotFoundException($"Application location '{location}' not found.");

            var hApp = Native.LoadApplication(location, (int)mode);

            return new App(hApp, Process.GetCurrentProcess().Id);
        }

        public virtual IApp LoadApp(string corLocation, string appLocation, LoadRunMode mode)
        {
            AddCorLocation(corLocation);

            return LoadApp(appLocation, mode);
        }
    };
}