﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using Server.Core.Exceptions;
using Server.Core.Extensions;

namespace Server.Core.NativeAPI
{
    internal static partial class Native
    {
        private const string API_LIB_NAME = "NativeAPI.dll";

        [Flags]
        public enum LoadLibraryFlags : uint
        {
            NONE = 0x00000000,
            DONT_RESOLVE_DLL_REFERENCES = 0x00000001,
            LOAD_IGNORE_CODE_AUTHZ_LEVEL = 0x00000010,
            LOAD_LIBRARY_AS_DATAFILE = 0x00000002,
            LOAD_LIBRARY_AS_DATAFILE_EXCLUSIVE = 0x00000040,
            LOAD_LIBRARY_AS_IMAGE_RESOURCE = 0x00000020,
            LOAD_LIBRARY_SEARCH_APPLICATION_DIR = 0x00000200,
            LOAD_LIBRARY_SEARCH_DEFAULT_DIRS = 0x00001000,
            LOAD_LIBRARY_SEARCH_DLL_LOAD_DIR = 0x00000100,
            LOAD_LIBRARY_SEARCH_SYSTEM32 = 0x00000800,
            LOAD_LIBRARY_SEARCH_USER_DIRS = 0x00000400,
            LOAD_WITH_ALTERED_SEARCH_PATH = 0x00000008
        };

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern IntPtr AddDllDirectory(string dirPath);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool SetDllDirectory(string dirPath);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool RemoveDllDirectory(IntPtr dirCookie);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool SetDefaultDllDirectories(LoadLibraryFlags loadFlags);

        public enum CorEventType
        {
            Sent = 0,
            Cleared = 1,
            Count
        };

        [StructLayout(LayoutKind.Sequential)]
        public struct CorMsgEventArgs
        {
            public int Size;
            public int Level;
            public int Category;
            public long TimeStamp;

            [MarshalAs(UnmanagedType.LPWStr)]
            public string Text;
        };

        public delegate void CorMsgHandler(int hModel, CorEventType eventType, CorMsgEventArgs eventArgs, IntPtr userData);

        public enum ResultCode
        {
            SUCCESS_OK = 0,
            SUCCESS_SIZE_RETRIEVED = 1,

            ERROR_FAIL = -1,
            ERROR_BAD_PARAMETER = -2,
            ERROR_INSUFFICIENT_BUFFER = -3,
            ERROR_NOT_IMPLEMENTED = -4,
            ERROR_INDEX_OUT_OF_RANGE = -5,
            ERROR_PARAMETER_IS_NULL = -6,
            ERROR_INVALID_OPERATION = -7,
            ERROR_INVALID_CAST = -8,
            ERROR_NOT_FINITE_NUMBER = -9,
            ERROR_OVERFLOW = -10,
            ERROR_DIVIDE_BY_ZERO = -11,
            ERROR_INVALID_FORMAT = -12,
            ERROR_OUT_OF_MEMORY = -13,
            ERROR_LOAD_SAVE = -14,
            ERROR_FILE = -15,
            ERROR_DB_INTEGRITY_VIOLATION = -16,
            ERROR_DB_LINK_REFERENCE_TYPE_MISMATCH = -17,
            ERROR_DB_OBJECT_NAME_NOT_UNIQUE = -18,
            ERROR_DB_EXECUTE_RULESET_EVENT = -19,
            ERROR_DB_CHECK = -20,
            ERROR_DB_SOLVE = -21,
            ERROR_DIAGRAM_INTEGRITY_VIOLATION = -22,
            ERROR_NAME_NOT_UNIQUE = -23,
            ERROR_COLLECTION_INTEGRIY_VIOLATION = -24,
            ERROR_DATA_CONNECTIVITY = -25,
            ERROR_QUERY_EXECUTION = -26,
            ERROR_QUERY_PARAMETER_NOT_SUPPLIED = -27,
            ERROR_QUERY_EXPRESSION_NOT_CONSTANT = -28,
            ERROR_SQL_PARSE = -29,
            ERROR_NO_MORE_HANDLES = -30,
            ERROR_INVALID_HANDLE = -31,
            ERROR_ITEM_NOT_FOUND = -32,
            ERROR_UNSPECIFIED_OBJECT_NAME = -33,
            ERROR_METHOD_NOT_SUPPORTED = -34,
            ERROR_ATTEMPT_TO_EXECUTE_FROM_WRONG_THREAD = -35,
            ERROR_APPLICATION_INIT = -36,
            ERROR_LOADING_APPLICATION_FAILED = -37
        };

        public static bool SUCCESS(int code, out ResultCode resultCode)
        {
            resultCode = code.ToEnum<ResultCode>();

            return (resultCode == ResultCode.SUCCESS_OK) || (resultCode == ResultCode.SUCCESS_SIZE_RETRIEVED);
        }

        public static bool FAILURE(int code, out ResultCode resultCode)
        {
            return !SUCCESS(code, out resultCode);
        }

        public static Exception ERROR(ResultCode resultCode)
        {
            return ERROR(resultCode, null);
        }

        public static Exception ERROR(ResultCode resultCode, string message)
        {
            return ERROR(resultCode, message, null);
        }

        public static Exception ERROR(ResultCode resultCode, string message, Exception innerError)
        {
            switch (resultCode)
            {
                case ResultCode.ERROR_FAIL:
                    return new FailException(message, innerError);

                case ResultCode.ERROR_BAD_PARAMETER:
                    return new ArgumentException(message, innerError);

                case ResultCode.ERROR_INSUFFICIENT_BUFFER:
                    return new InsufficientMemoryException(message, innerError);

                case ResultCode.ERROR_NOT_IMPLEMENTED:
                    return new NotImplementedException(message, innerError);

                case ResultCode.ERROR_INDEX_OUT_OF_RANGE:
                    return new IndexOutOfRangeException(message, innerError);

                case ResultCode.ERROR_PARAMETER_IS_NULL:
                    return new NullReferenceException(message, innerError);

                case ResultCode.ERROR_INVALID_OPERATION:
                    return new InvalidOperationException(message, innerError);

                case ResultCode.ERROR_INVALID_CAST:
                    return new InvalidCastException(message, innerError);

                case ResultCode.ERROR_NOT_FINITE_NUMBER:
                    return new NotFiniteNumberException(message, innerError);

                case ResultCode.ERROR_OVERFLOW:
                    return new OverflowException(message, innerError);

                case ResultCode.ERROR_DIVIDE_BY_ZERO:
                    return new DivideByZeroException(message, innerError);

                case ResultCode.ERROR_INVALID_FORMAT:
                    return new FormatException(message, innerError);

                case ResultCode.ERROR_OUT_OF_MEMORY:
                    return new OutOfMemoryException(message, innerError);

                case ResultCode.ERROR_LOAD_SAVE:
                    return new LoadSaveException(message, innerError);

                case ResultCode.ERROR_FILE:
                    return new IOException(message, innerError);

                case ResultCode.ERROR_DB_INTEGRITY_VIOLATION:
                    return new DbIntegrityViolationException(message, innerError);

                case ResultCode.ERROR_DB_LINK_REFERENCE_TYPE_MISMATCH:
                    return new DbLinkReferenceTypeMismatchException(message, innerError);

                case ResultCode.ERROR_DB_OBJECT_NAME_NOT_UNIQUE:
                    return new DbObjectNameNotUniqueException(message, innerError);

                case ResultCode.ERROR_DB_EXECUTE_RULESET_EVENT:
                    return new DbExecuteRulesetEventException(message, innerError);

                case ResultCode.ERROR_DB_CHECK:
                    return new DbCheckException(message, innerError);

                case ResultCode.ERROR_DB_SOLVE:
                    return new DbSolveException(message, innerError);

                case ResultCode.ERROR_DIAGRAM_INTEGRITY_VIOLATION:
                    return new DiagramIntegrityViolationException(message, innerError);

                case ResultCode.ERROR_NAME_NOT_UNIQUE:
                    return new ModelItemNameNotUniqueException(message, innerError);

                case ResultCode.ERROR_COLLECTION_INTEGRIY_VIOLATION:
                    return new ModelCollectionIntegrityViolationException(message, innerError);

                case ResultCode.ERROR_DATA_CONNECTIVITY:
                    return new DataConnectivityException(message, innerError);

                case ResultCode.ERROR_QUERY_EXECUTION:
                    return new QueryExecutionException(message, innerError);

                case ResultCode.ERROR_QUERY_PARAMETER_NOT_SUPPLIED:
                    return new QueryParameterNotSuppliedException(message, innerError);

                case ResultCode.ERROR_QUERY_EXPRESSION_NOT_CONSTANT:
                    return new QueryExpressionNotConstantException(message, innerError);

                case ResultCode.ERROR_SQL_PARSE:
                    return new SqlParseException(message, innerError);

                case ResultCode.ERROR_NO_MORE_HANDLES:
                    return new NoMoreHandlesException(message, innerError);

                case ResultCode.ERROR_INVALID_HANDLE:
                    return new InvalidHandleException(message, innerError);

                case ResultCode.ERROR_ITEM_NOT_FOUND:
                    return new ItemNotFoundException(message, innerError);

                case ResultCode.ERROR_UNSPECIFIED_OBJECT_NAME:
                    return new UnspecifiedObjectNameException(message, innerError);

                case ResultCode.ERROR_METHOD_NOT_SUPPORTED:
                    return new NotSupportedException(message, innerError);

                case ResultCode.ERROR_ATTEMPT_TO_EXECUTE_FROM_WRONG_THREAD:
                    return new AttemptToExecuteFromWrongThreadException(message, innerError);

                case ResultCode.ERROR_APPLICATION_INIT:
                    return new ApplicationInitException(message, innerError);

                case ResultCode.ERROR_LOADING_APPLICATION_FAILED:
                    return new ApplicationLoadException(message, innerError);

                default:
                    return new ExternalException(message, innerError);
            }
        }
    };
}
