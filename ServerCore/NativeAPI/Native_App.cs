﻿using System.Runtime.InteropServices;
using System.Text;

namespace Server.Core.NativeAPI
{
    static partial class Native
    {
        #region LoadApplication

        [DllImport(API_LIB_NAME, CharSet = CharSet.Unicode, SetLastError = true)]
        private static extern int CORELoadApplication(string location, int mode, out int hApp);

        public static int LoadApplication(string location, int mode)
        {
            if (FAILURE(CORELoadApplication(location, mode, out int hApp), out ResultCode resultCode))
                throw ERROR(resultCode, $"Application locatded at '{location}' is not initialized.");
            return hApp;
        }

        #endregion LoadApplication

        #region CloseApplication

        [DllImport(API_LIB_NAME, CharSet = CharSet.Auto, SetLastError = true, EntryPoint = "COREExit")]
        private static extern int CORECloseApplication(int hApp);

        public static void CloseApplication(int hApp)
        {
            if (FAILURE(CORECloseApplication(hApp), out _))
                throw ERROR(ResultCode.ERROR_INVALID_HANDLE, "Application handle is invalid.");
        }

        #endregion CloseApplication

        #region GetApplicationLocation

        [DllImport(API_LIB_NAME, CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int COREGetApplicationLocation(int hApp, StringBuilder buffer, int bufferSize, out int actBufferSize);

        public static string GetApplicationLocation(int hApp)
        {
            const string message = "Cannot get application name.";

            if (FAILURE(COREGetApplicationLocation(hApp, null, 0, out int capacity), out ResultCode resultCode))
                throw ERROR(resultCode, message);

            var buffer = new StringBuilder(capacity);
            if (FAILURE(COREGetApplicationLocation(hApp, buffer, buffer.Capacity, out _), out resultCode))
                throw ERROR(resultCode, message);

            return buffer.ToString();
        }

        #endregion GetApplicationLocation

        #region GetApplicationName

        [DllImport(API_LIB_NAME, CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int COREGetApplicationName(int hApp, StringBuilder buffer, int bufferSize, out int actBufferSize);

        public static string GetApplicationName(int hApp)
        {
            const string message = "Cannot get application name.";

            if (FAILURE(COREGetApplicationName(hApp, null, 0, out int capacity), out ResultCode resultCode))
                throw ERROR(resultCode, message);

            var buffer = new StringBuilder(capacity);
            if (FAILURE(COREGetApplicationName(hApp, buffer, buffer.Capacity, out _), out resultCode))
                throw ERROR(resultCode, message);

            return buffer.ToString();
        }

        #endregion GetApplicationName

        #region GetApplicationVersion

        [DllImport(API_LIB_NAME, CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int COREGetApplicationVersion(int hApp, StringBuilder buffer, int bufferSize, out int actBufferSize);

        public static string GetApplicationVersion(int hApp)
        {
            const string message = "Cannot get application version.";

            if (FAILURE(COREGetApplicationVersion(hApp, null, 0, out int capacity), out ResultCode resultCode))
                throw ERROR(resultCode, message);

            var buffer = new StringBuilder(capacity);
            if (FAILURE(COREGetApplicationVersion(hApp, buffer, buffer.Capacity, out _), out resultCode))
                throw ERROR(resultCode, message);

            return buffer.ToString();
        }

        #endregion GetApplicationVersion

        #region GetApplicationEdition

        [DllImport(API_LIB_NAME, CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int COREGetApplicationEdition(int hApp, StringBuilder buffer, int bufferSize, out int actBufferSize);

        public static string GetApplicationEdition(int hApp)
        {
            const string message = "Cannot get application verion.";

            if (FAILURE(COREGetApplicationEdition(hApp, null, 0, out int capacity), out ResultCode resultCode))
                throw ERROR(resultCode, message);

            var buffer = new StringBuilder(capacity);
            if (FAILURE(COREGetApplicationEdition(hApp, buffer, buffer.Capacity, out _), out resultCode))
                throw ERROR(resultCode, message);

            return buffer.ToString();
        }

        #endregion GetApplicationEdition

        #region GetApplicationInfo

        [DllImport(API_LIB_NAME, CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int COREGetApplicationInfo(int hApp, StringBuilder buffer, int bufferSize, out int actBufferSize);

        public static string GetApplicationInfo(int hApp)
        {
            const string message = "Cannot get application info.";

            if (FAILURE(COREGetApplicationInfo(hApp, null, 0, out int capacity), out ResultCode resultCode))
                throw ERROR(resultCode, message);

            var buffer = new StringBuilder(capacity);
            if (FAILURE(COREGetApplicationInfo(hApp, buffer, buffer.Capacity, out _), out resultCode))
                throw ERROR(resultCode, message);

            return buffer.ToString();
        }

        #endregion GetApplicationInfo

        #region GetEngineVersion

        [DllImport(API_LIB_NAME, CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int COREGetEngineVersion(int hApp, StringBuilder buffer, int bufferSize, out int actBufferSize);

        public static string GetEngineVersion(int hApp)
        {
            const string message = "Cannot get application engine version.";

            if (FAILURE(COREGetEngineVersion(hApp, null, 0, out int capacity), out ResultCode resultCode))
                throw ERROR(resultCode, message);

            var buffer = new StringBuilder(capacity);
            if (FAILURE(COREGetEngineVersion(hApp, buffer, buffer.Capacity, out _), out resultCode))
                throw ERROR(resultCode, message);

            return buffer.ToString();
        }

        #endregion GetEngineVersion

        #region GetSchemaSignature

        [DllImport(API_LIB_NAME, CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int COREGetSchemaSignature(int hApp, StringBuilder buffer, int bufferSize, out int actBufferSize);

        public static string GetSchemaSignature(int hApp)
        {
            const string message = "Cannot get application schema signature.";

            if (FAILURE(COREGetSchemaSignature(hApp, null, 0, out int capacity), out ResultCode resultCode))
                throw ERROR(resultCode, message);

            var buffer = new StringBuilder(capacity);
            if (FAILURE(COREGetSchemaSignature(hApp, buffer, buffer.Capacity, out _), out resultCode))
                throw ERROR(resultCode, message);

            return buffer.ToString();
        }

        #endregion GetSchemaSignature
    };
}