using Server.Core.Schema;
using System;
using System.IO;
using Xunit;

namespace ServerCore.Tests
{
    public class CoreTests
    {
        [Fact]
        public void App_LoadClose_Success()
        {
            var loader = new AppLoader();
            var app = loader.LoadApp(Environment.CurrentDirectory, LoadRunMode.Default);

            Assert.Equal(Environment.CurrentDirectory, app.Location);
            Assert.Equal($"App{1}", app.Name);
            Assert.Equal(new Version("1.0"), app.Version);
            Assert.Equal("TestEdition1", app.Edition);
            Assert.Equal("TestInfo(App1)", app.Info);
            Assert.Equal(new Version("1.0"), app.EngineVersion);
            Assert.Equal("App1Signature", app.SchemaSignature);
            
            app.Close();
        }
    }
}
