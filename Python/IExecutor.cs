﻿using System;

namespace INFOPRO.Examples.Python
{
    public interface IExecutor<T> : IDisposable
    {
        void Initialize(T arg);

        void Execute();
    };

    public interface IExecutor<T1, T2> : IDisposable
    {
        void Initialize(T1 arg1, T2 arg2);

        void Execute();
    };
}