﻿namespace INFOPRO.Examples.Python.Constants
{
    public static class XmlParameterTraits
    {
        public static class Names
        {
            public static string Engine = "Engine";
            public static string Script = "Script";
            public static string InputVariables = "InputVariables";
            public static string OutputVariables = "OutputVariables";
            public static string SearchPaths = "SearchPaths";
            public static string InitCommands = "InitCommands";
            public static string EngineFile = "EngineFile";
        };
    };
}