﻿namespace INFOPRO.Examples.Python.Constants
{
    public enum JobMessageSource
    {
        Agent,
        App,
        API,
        SSIS,
        POSH,
        MSQL,
        PSQL
    }
}