﻿namespace INFOPRO.Examples.Python.Constants
{
    public enum JobMessageKind
    {
        Info,
        Error,
        Warning
    }
}