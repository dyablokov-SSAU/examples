﻿using INFOPRO.Examples.Python.Constants;
using INFOPRO.Examples.Python.Xml;
using Microsoft.Scripting.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using IPH = IronPython.Hosting;

namespace INFOPRO.Examples.Python
{
    internal class IronPythonExecutor : IExecutor<XmlParameterCollection>
    {
        private readonly IJob _job;

        private string _script;
        private ScriptEngine _engine;
        private ScriptScope _scope;
        private List<string> _outputVars = new List<string>();
        private MemoryStream _outputStream = new MemoryStream();

        public IronPythonExecutor(IJob job)
        {
            _job = job ?? throw new ArgumentNullException(nameof(job));
        }

        private void CreateEngine()
        {
            _engine = IPH.Python.CreateEngine();

            var errorStream = new MemoryStream();
            var outputStream = new MemoryStream();
            var errorWriter = new IronPythonStreamWriter(errorStream);
            var outputWriter = new IronPythonStreamWriter(outputStream);
            errorWriter.StreamWrite += ErrorMessageReceived;
            outputWriter.StreamWrite += OutputMessageReceived;

            _engine.Runtime.IO.SetErrorOutput(errorStream, errorWriter);
            _engine.Runtime.IO.SetOutput(outputStream, outputWriter);
        }

        private void CreateScope()
        {
            _scope = _engine.CreateScope();
        }

        private void ParseScript(XmlParameterCollection xmlParams)
        {
            var scriptParam = xmlParams.Where(item =>
            {
                if (item.Attributes.TryGetValue("Name", out var name))
                {
                    if (string.Compare(name, XmlParameterTraits.Names.Script, true) == 0)
                        return true;
                }
                return false;
            }).FirstOrDefault();

            if (scriptParam is null)
                throw new Exception($"Parameter with name '{XmlParameterTraits.Names.Script}' not found.");

            _script = Convert.ToString(scriptParam.Value);
        }

        private void ParseInputVariables(XmlParameterCollection xmlParams)
        {
            var inputVarsParam = xmlParams.Where(item =>
            {
                if (item.Attributes.TryGetValue("Name", out var name))
                {
                    if (string.Compare(name, XmlParameterTraits.Names.InputVariables, true) == 0)
                        return true;
                }
                return false;
            }).FirstOrDefault();

            if (inputVarsParam is null)
                return;

            var inputVarNames = Convert.ToString(inputVarsParam.Value)
                .Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                .Select(n => n.Trim());

            var jobVarNames = _job.GetVariableNames()
                .Where(jvn => inputVarNames.Any(vn => string.Compare(jvn, vn, true) == 0));

            foreach (var jobVarName in jobVarNames)
                _scope.SetVariable(jobVarName, _job.GetVariableValue(jobVarName));
        }

        private void ParseOutputVariables(XmlParameterCollection xmlParams)
        {
            var outputVarsParam = xmlParams.Where(item =>
            {
                if (item.Attributes.TryGetValue("Name", out var name))
                {
                    if (string.Compare(name, XmlParameterTraits.Names.OutputVariables, true) == 0)
                        return true;
                }
                return false;
            }).FirstOrDefault();

            if (outputVarsParam is null)
                return;

            var outVarNames = Convert.ToString(outputVarsParam.Value)
                .Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                .Select(vn => vn.Trim());

            _outputVars.AddRange(outVarNames);

            foreach (var outputVar in _outputVars.Except(_scope.GetVariableNames()))
                _scope.SetVariable(outputVar, string.Empty);
        }

        private void ParseSearchPaths(XmlParameterCollection xmlParams)
        {
            var searchPathParam = xmlParams.Where(item =>
            {
                if (item.Attributes.TryGetValue("Name", out var name))
                {
                    if (string.Compare(name, XmlParameterTraits.Names.SearchPaths, true) == 0)
                        return true;
                }
                return false;
            }).FirstOrDefault();

            if (searchPathParam is null)
                return;

            var paramValue = Convert.ToString(searchPathParam.Value);

            if (string.IsNullOrEmpty(paramValue))
                return;

            var searchPaths = paramValue
                .Split(";".ToCharArray())
                .Select(sp => sp.Trim());

            var invalidPaths = searchPaths
                .Where(sp => !Directory.Exists(sp));

            if (invalidPaths.Any())
                throw new DirectoryNotFoundException($"Directory '{invalidPaths.First()}' not found.");

            _engine.SetSearchPaths(searchPaths.ToArray());
        }

        private void ExtractOutputVariables()
        {
            foreach (var name in _outputVars)
            {
                var value = _scope.GetVariable(name);
                _job.CreateOrUpdateVariable(name, Convert.ToString(value));
            }
        }

        private void ErrorMessageReceived(object sender, IronPythonStreamWriterEventArgs args)
        {
            var message = Convert.ToString(args.Value);

            if (string.IsNullOrEmpty(message) || string.Compare(message, Environment.NewLine, true) == 0)
                return;

            _job.CreateMessage(JobMessageKind.Error, JobMessageSource.Agent, message);
        }

        private void OutputMessageReceived(object sender, IronPythonStreamWriterEventArgs args)
        {
            var message = Convert.ToString(args.Value);

            if (string.IsNullOrEmpty(message) || string.Compare(message, Environment.NewLine, true) == 0)
                return;

            _job.CreateMessage(JobMessageKind.Info, JobMessageSource.Agent, message);
        }

        public virtual void Initialize(XmlParameterCollection xmlParams)
        {
            if (xmlParams is null)
                throw new ArgumentNullException(nameof(xmlParams));

            try
            {
                CreateEngine();
            }
            catch (Exception error)
            {
                throw new Exception($"Python engine not initialized! Error = '{error.Message}'");
            }

            try
            {
                CreateScope();
            }
            catch (Exception error)
            {
                throw new Exception($"Python scope not created! Error = '{error.Message}'");
            }

            try
            {
                ParseScript(xmlParams);
                ParseInputVariables(xmlParams);
                ParseOutputVariables(xmlParams);
                ParseSearchPaths(xmlParams);
            }
            catch (Exception error)
            {
                throw new Exception($"Parameters not initialized! Error = '{error.Message}'");
            }
        }

        public virtual void Execute()
        {
            try
            {
                _engine?.Execute(_script, _scope);

                ExtractOutputVariables();
            }
            catch (Exception error)
            {
                throw new Exception($"Python script can not be executed! Error = '{error.Message}'");
            }
        }

        public virtual void Dispose()
        {
        }
    };

    internal abstract class IronPythonStreamWriterEventArgs : EventArgs
    {
        public abstract Type ValueType { get; }

        public IronPythonStreamWriterEventArgs(object value)
        {
            Value = value;
        }

        public virtual object Value { get; }
    };

    internal class IronPythonStreamWriterEventArgs<T> : IronPythonStreamWriterEventArgs
    {
        public IronPythonStreamWriterEventArgs(object value) : base(value)
        {
        }

        public override Type ValueType => typeof(T);
    };

    internal class IronPythonStreamWriter : StreamWriter
    {
        public event EventHandler<IronPythonStreamWriterEventArgs> StreamWrite;

        public IronPythonStreamWriter(Stream stream) : base(stream)
        {
        }

        public void OnEvent<T>(object value)
        {
            var handler = StreamWrite;
            if (handler is null)
                return;

            handler(this, new IronPythonStreamWriterEventArgs<T>(value));
        }

        public override void Write(bool value)
        {
            base.Write(value);
            OnEvent<bool>(value);
        }

        public override void Write(char value)
        {
            base.Write(value);
            OnEvent<char>(value);
        }

        public override void Write(char[] buffer)
        {
            base.Write(buffer);
            OnEvent<char[]>(buffer);
        }

        public override void Write(char[] buffer, int index, int count)
        {
            base.Write(buffer, index, count);
            var segment = new ArraySegment<char>(buffer, index, count);
            OnEvent<char[]>(segment.Array);
        }

        public override void Write(decimal value)
        {
            base.Write(value);
            OnEvent<decimal>(value);
        }

        public override void Write(double value)
        {
            base.Write(value);
            OnEvent<double>(value);
        }

        public override void Write(float value)
        {
            base.Write(value);
            OnEvent<float>(value);
        }

        public override void Write(int value)
        {
            base.Write(value);
            OnEvent<int>(value);
        }

        public override void Write(long value)
        {
            base.Write(value);
            OnEvent<long>(value);
        }

        public override void Write(object value)
        {
            base.Write(value);
            OnEvent<object>(value);
        }

        public override void Write(string format, object arg0)
        {
            base.Write(format, arg0);
            OnEvent<string>(string.Format(format, arg0));
        }

        public override void Write(string format, object arg0, object arg1)
        {
            base.Write(format, arg0, arg1);
            OnEvent<string>(string.Format(format, arg0, arg1));
        }

        public override void Write(string format, object arg0, object arg1, object arg2)
        {
            base.Write(format, arg0, arg1, arg2);
            OnEvent<string>(string.Format(format, arg0, arg1, arg2));
        }

        public override void Write(string format, params object[] args)
        {
            base.Write(format, args);
            OnEvent<string>(string.Format(format, args));
        }

        public override void Write(string value)
        {
            base.Write(value);
            OnEvent<string>(value);
        }

        public override void Write(uint value)
        {
            base.Write(value);
            OnEvent<uint>(value);
        }

        public override void Write(ulong value)
        {
            base.Write(value);
            OnEvent<ulong>(value);
        }
    };
}