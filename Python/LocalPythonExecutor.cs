﻿using INFOPRO.Examples.Python.Constants;
using INFOPRO.Examples.Python.Xml;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace INFOPRO.Examples.Python
{
    internal class LocalPythonExecutor : IExecutor<XmlParameterCollection>
    {
        private readonly IJob _job;

        private Process _engineProcess;
        private List<string> _outputVarNames;

        public LocalPythonExecutor(IJob job)
        {
            _job = job ?? throw new ArgumentNullException(nameof(job));
        }

        private string GetWorkingPath()
        {
            return Path.GetTempPath();
        }

        private string GetTempPythonFileName()
        {
            return Path.ChangeExtension(_job.Id.ToString("N"), "py");
        }

        private string GetTempOutputVariablesFileName()
        {
            return Path.ChangeExtension(_job.Id.ToString("N"), "ovv");
        }

        private string GetTempPythonFilePath()
        {
            return Path.Combine(GetWorkingPath(), GetTempPythonFileName());
        }

        private string GetTempOutputVariablesFilePath()
        {
            return Path.Combine(GetWorkingPath(), GetTempOutputVariablesFileName());
        }

        private void PrepareTempPythonFile(params string[] lines)
        {
            using (var stream = new FileStream(GetTempPythonFilePath(), FileMode.Create | FileMode.Append, FileAccess.Write))
            {
                using (var writer = new StreamWriter(stream))
                {
                    foreach (var line in lines ?? new string[0])
                    {
                        writer.WriteLine(line);
                    }
                }
            }
        }

        private void PrepareArguments(params string[] lines)
        {
            foreach (var line in lines ?? new string[0])
            {
                if (string.IsNullOrEmpty(_engineProcess.StartInfo.Arguments))
                {
                    _engineProcess.StartInfo.Arguments = $"/C {line}";
                    continue;
                }

                _engineProcess.StartInfo.Arguments += $" && {line}";
            }
        }

        private void CreateEngine()
        {
            _engineProcess = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "cmd.exe",
                    UseShellExecute = false,
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    CreateNoWindow = true,
                    WorkingDirectory = GetWorkingPath()
                },
                EnableRaisingEvents = true
            };

            _engineProcess.ErrorDataReceived += ErrorDataReceived;
            _engineProcess.OutputDataReceived += OutputDataReceived;
        }

        private void ErrorDataReceived(object sender, DataReceivedEventArgs args)
        {
            if (string.IsNullOrEmpty(args.Data))
                return;

            _job.CreateMessageAsync(JobMessageKind.Error, JobMessageSource.Agent, args.Data).GetAwaiter().GetResult();
        }

        private void OutputDataReceived(object sender, DataReceivedEventArgs args)
        {
            if (string.IsNullOrEmpty(args.Data))
                return;

            _job.CreateMessageAsync(JobMessageKind.Info, JobMessageSource.Agent, args.Data).GetAwaiter().GetResult();
        }

        private void ParseInputVariables(XmlParameterCollection xmlParams)
        {
            var xmlParam = xmlParams.Where(item =>
            {
                if (item.Attributes.TryGetValue("Name", out var name))
                {
                    if (string.Compare(name, XmlParameterTraits.Names.InputVariables, true) == 0)
                        return true;
                }
                return false;
            }).FirstOrDefault();

            if (xmlParam is null)
                return;

            var inputVarNames = Convert.ToString(xmlParam.Value)
                .Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                .Select(n => n.Trim());

            var jobVarNames = _job.GetVariableNamesAsync().GetAwaiter().GetResult()
                .Where(jvn => inputVarNames.Any(vn => string.Compare(jvn, vn, true) == 0));

            foreach (var jobVarName in jobVarNames)
                PrepareTempPythonFile($"{jobVarName} = {_job.GetVariableValue(jobVarName)}");
        }

        private void ParseScript(XmlParameterCollection xmlParams)
        {
            var xmlParam = xmlParams.Where(item =>
            {
                if (item.Attributes.TryGetValue("Name", out var name))
                {
                    if (string.Compare(name, XmlParameterTraits.Names.Script, true) == 0)
                        return true;
                }
                return false;
            }).FirstOrDefault();

            if (xmlParam is null)
                throw new Exception($"Parameter with name '{XmlParameterTraits.Names.Script}' not found.");

            PrepareTempPythonFile(Convert.ToString(xmlParam.Value));
        }

        private void ParseOutputVariables(XmlParameterCollection xmlParams)
        {
            var xmlParam = xmlParams.Where(item =>
            {
                if (item.Attributes.TryGetValue("Name", out var name))
                {
                    if (string.Compare(name, XmlParameterTraits.Names.OutputVariables, true) == 0)
                        return true;
                }
                return false;
            }).FirstOrDefault();

            if (xmlParam is null)
                return;

            _outputVarNames = Convert.ToString(xmlParam.Value)
                .Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                .Select(vn => vn.Trim())
                .ToList();

            if (_outputVarNames.Count == 0)
                return;

            var builder = new StringBuilder();
            builder.AppendLine($"with open(r'{GetTempOutputVariablesFilePath()}', 'w') as f:");

            foreach (var outputVarName in _outputVarNames)
                builder.Append("\t").AppendLine($"f.write(str({{'{outputVarName}':{outputVarName}}}))");

            PrepareTempPythonFile(builder.ToString());
        }

        private void ParseInitCommands(XmlParameterCollection xmlParams)
        {
            var xmlParam = xmlParams.Where(item =>
            {
                if (item.Attributes.TryGetValue("Name", out var name))
                {
                    if (string.Compare(name, XmlParameterTraits.Names.InitCommands, true) == 0)
                        return true;
                }
                return false;
            }).FirstOrDefault();

            if (xmlParam is null)
                return;

            var initCommands = Convert.ToString(xmlParam.Value)
                .Split(new[] { "\n", "\r\n", ";" }, StringSplitOptions.RemoveEmptyEntries)
                .Select(ic => ic.Trim())
                .ToArray();

            PrepareArguments(initCommands);
        }

        private void ParseEngineFile(XmlParameterCollection xmlParams)
        {
            var xmlParam = xmlParams.Where(item =>
            {
                if (item.Attributes.TryGetValue("Name", out var name))
                {
                    if (string.Compare(name, XmlParameterTraits.Names.EngineFile, true) == 0)
                        return true;
                }
                return false;
            }).FirstOrDefault();

            if (xmlParam is null)
                throw new Exception($"Parameter with name '{XmlParameterTraits.Names.Script}' not found.");

            var engineFile = Convert.ToString(xmlParam.Value);

            PrepareArguments($"{engineFile} {GetTempPythonFilePath()}");
        }

        private void ExtractOutputVariables()
        {
            if (!File.Exists(GetTempOutputVariablesFilePath()))
                return;

            string data;

            using (var stream = new FileStream(GetTempOutputVariablesFilePath(), FileMode.Open, FileAccess.Read))
            {
                using (var reader = new StreamReader(stream))
                {
                    data = reader.ReadToEnd();
                }
            }

            foreach (var ovn in _outputVarNames)
            {
                var pattern = $@"(?<=\{{'{ovn}'\:)[^}}{{]*(\s*|\S*)*(?=\}})";
                var match = Regex.Match(data, pattern);

                if (!match.Success)
                    continue;

                _job.CreateOrUpdateVariable(ovn, match.Value);
            }
        }

        public virtual void Initialize(XmlParameterCollection xmlParams)
        {
            if (xmlParams is null)
                throw new ArgumentNullException(nameof(xmlParams));

            try
            {
                CreateEngine();
            }
            catch (Exception error)
            {
                throw new Exception($"Python engine not initialized! Error = '{error.Message}'");
            }

            try
            {
                if (File.Exists(GetTempPythonFilePath()))
                    File.Delete(GetTempPythonFilePath());

                ParseInputVariables(xmlParams);
                ParseScript(xmlParams);
                ParseOutputVariables(xmlParams);
                ParseInitCommands(xmlParams);
                ParseEngineFile(xmlParams);
            }
            catch (Exception error)
            {
                throw new Exception($"Parameters not initialized! Error = '{error.Message}'");
            }
        }

        public virtual void Execute()
        {
            try
            {
                _engineProcess.Start();
                _engineProcess.BeginErrorReadLine();
                _engineProcess.BeginOutputReadLine();
                _engineProcess.WaitForExit();
            }
            catch (Exception error)
            {
                throw new Exception($"Python script can not be executed! Error = '{error.Message}'");
            }

            try
            {
                ExtractOutputVariables();
            }
            catch(Exception error)
            {
                throw new Exception($"Failed to fetch output variables! Error = '{error.Message}'");
            }
        }

        public virtual void Dispose()
        {
            if (!_engineProcess.HasExited)
                _engineProcess.Kill();

            if (File.Exists(GetTempPythonFilePath()))
                File.Delete(GetTempPythonFilePath());

            if (File.Exists(GetTempOutputVariablesFilePath()))
                File.Delete(GetTempOutputVariablesFilePath());
        }
    };
}
