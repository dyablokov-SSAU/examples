﻿using INFOPRO.Examples.Python.Constants;
using INFOPRO.Examples.Python.Xml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace INFOPRO.Examples.Python
{
    public class PythonExecutor : IExecutor<IJob, string>
    {
        private IExecutor<XmlParameterCollection> _executor;

        private ScriptShell ParseEngine(XmlParameterCollection xmlParams)
        {
            const string engineParamName = "Engine";

            var xmlParam = xmlParams.Where(item =>
            {
                if (item.Attributes.TryGetValue("Name", out var name))
                {
                    if (string.Compare(name, engineParamName, true) == 0)
                        return true;
                }
                return false;
            }).FirstOrDefault();

            if (xmlParam is null)
                return ScriptShell.IronPython;
            
            if (!Enum.TryParse<ScriptShell>(Convert.ToString(xmlParam.Value), out var result))
                throw new FormatException($"Parameter with name '{engineParamName}' has an invalid format.");

            return result;
        }

        public void Dispose()
        {
            _executor.Dispose();
        }

        public void Execute()
        {
            _executor.Execute();
        }

        public void Initialize(IJob job, string parameters)
        {
            if (job is null)
                throw new ArgumentNullException(nameof(job));

            if (string.IsNullOrEmpty(parameters))
                throw new ArgumentException($"Argument '{parameters}' cannot be null or empty.");

            if (!XmlHelper.DeserializeFromString(parameters, out XmlParameterCollection xmlParams, false))
                throw new FormatException($"Argument '{nameof(parameters)}' has invalide format.");
            
            var engine = ParseEngine(xmlParams);

            if (engine == ScriptShell.IronPython)
                _executor = new IronPythonExecutor(job);
            else
                _executor = new LocalPythonExecutor(job);

            _executor.Initialize(xmlParams);
        }
    };
}
