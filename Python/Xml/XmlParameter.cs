﻿using System;
using System.Xml;
using System.Xml.Serialization;

namespace INFOPRO.Examples.Python.Xml
{
    [XmlRoot(ElementName = Tag)]
    public class XmlParameter : XmlSerializable
    {
        public const string Tag = "Parameter";
        private const string _typeNameTag = "TypeName";
        private const string _readConverterPattern = "System.Converter`2[[System.String], [{0}]]";
        private const string _writeConverterPattern = "System.Converter`2[[{0}], [System.String]]";

        #region XmlSerializable implementation

        public override void ReadXml(XmlReader reader)
        {
            do
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        {
                            if (string.Compare(reader.Name, Tag, StringComparison.InvariantCultureIgnoreCase) == 0)
                            {
                                bool emptyElement = reader.IsEmptyElement;
                                Attributes.ReadXml(reader);
                                if (emptyElement)
                                    return;
                            }
                        }
                        break;

                    case XmlNodeType.Text:
                    case XmlNodeType.CDATA:
                        {
                            string typeName = null;

                            if (Attributes.TryGetValue(_typeNameTag, out typeName))
                            {
                                var valueType = Type.GetType(typeName, true, true);

                                if (string.Compare(valueType.FullName, typeof(string).FullName, StringComparison.InvariantCultureIgnoreCase) == 0)
                                    Value = reader.Value;
                                else
                                {
                                    var converterType = Type.GetType(string.Format(_readConverterPattern, valueType.FullName));

                                    var converter = Delegate.CreateDelegate(converterType, null, typeof(XmlConvert).GetMethod($"To{valueType.Name}", new Type[] { typeof(string) }));

                                    Value = converter.DynamicInvoke(reader.Value);
                                }
                            }
                        }
                        break;

                    case XmlNodeType.EndElement:
                        {
                            if (string.Compare(reader.Name, Tag, StringComparison.InvariantCultureIgnoreCase) == 0)
                                return;
                        }
                        break;

                    default: continue;
                }
            } while (reader.Read());
        }

        public override void WriteXml(XmlWriter writer)
        {
            string typeName = null;

            Attributes.WriteXml(writer);

            if (Value != null && Attributes.TryGetValue(_typeNameTag, out typeName))
            {
                var valueType = Type.GetType(typeName, true, true);

                if (string.Compare(valueType.FullName, typeof(string).FullName, true) == 0)
                    writer.WriteValue(Value);
                else
                {
                    var converterType = Type.GetType(string.Format(_writeConverterPattern, valueType.FullName));
                    var converter = Delegate.CreateDelegate(converterType, null, typeof(XmlConvert).GetMethod("ToString", new Type[] { valueType }));
                    writer.WriteValue(converter.DynamicInvoke(Value));
                }
            }
        }

        #endregion XmlSerializable implementation

        public XmlParameter()
        {
            Attributes = new XmlNameValueCollection();
        }

        public XmlParameter(string CDATA) : this(null, CDATA)
        {
        }

        public XmlParameter(object value) : this(null, value)
        {
        }

        public XmlParameter(XmlNameValueCollection attributes) : this(attributes, null)
        {
        }

        public XmlParameter(XmlNameValueCollection attributes, object value) : this()
        {
            if (attributes != null)
                Attributes.AddRange(attributes);

            if (value is null)
                return;

            Attributes.Add(_typeNameTag, value.GetType().FullName);
            Value = value;
        }

        public bool TryGetValue<T>(Converter<object, T> converter, out T value)
        {
            return TryGetValue(converter, out value, default(T));
        }

        public bool TryGetValue<T>(Converter<object, T> converter, out T value, T defaultValue)
        {
            value = defaultValue;

            if (converter is null)
                throw new ArgumentNullException(nameof(converter));

            if (Value != null)
            {
                value = converter(Value);
                return true;
            }

            return false;
        }

        public XmlNameValueCollection Attributes { get; private set; }

        public object Value { get; private set; }
    };
}