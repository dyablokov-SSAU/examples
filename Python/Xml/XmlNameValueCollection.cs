﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;

namespace INFOPRO.Examples.Python.Xml
{
    [XmlRoot(ElementName = Tag)]
    public class XmlNameValueCollection : XmlSerializable, IDictionary<string, string>
    {
        public const string Tag = "Attributes";
        private Dictionary<string, string> _sequence = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);

        #region IDictionary

        bool IDictionary<string, string>.ContainsKey(string key) => _sequence.ContainsKey(key);

        ICollection<string> IDictionary<string, string>.Keys => _sequence.Keys;

        public void Add(string name, string value) //add or update
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException($"Parameter {nameof(name)} cannot be null or empty");

            if (!_sequence.ContainsKey(name))
            {
                _sequence.Add(name, value);
                return;
            }

            _sequence[name] = value;
        }

        public bool Remove(string name) => _sequence.Remove(name);

        public bool TryGetValue(string name, out string value) => _sequence.TryGetValue(name, out value);

        public ICollection<string> Values => _sequence.Values;

        public string this[string name]
        {
            get => _sequence[name];
            set => _sequence[name] = value;
        }

        #endregion IDictionary

        #region ICollection

        void ICollection<KeyValuePair<string, string>>.Add(KeyValuePair<string, string> pair)
        {
            Add(pair.Key, pair.Value);
        }

        bool ICollection<KeyValuePair<string, string>>.Contains(KeyValuePair<string, string> pair)
        {
            return _sequence.ContainsKey(pair.Key) && _sequence.ContainsValue(pair.Value);
        }

        void ICollection<KeyValuePair<string, string>>.CopyTo(KeyValuePair<string, string>[] array, int arrayIndex)
        {
            ICollection<KeyValuePair<string, string>> collection = _sequence;
            collection.CopyTo(array, arrayIndex);
        }

        bool ICollection<KeyValuePair<string, string>>.Remove(KeyValuePair<string, string> item)
        {
            return _sequence.Remove(item.Key);
        }

        bool ICollection<KeyValuePair<string, string>>.IsReadOnly => false;

        public void Clear() => _sequence.Clear();

        public int Count => _sequence.Count;

        #endregion ICollection

        #region IEnumerable

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public IEnumerator<KeyValuePair<string, string>> GetEnumerator()
        {
            return _sequence.GetEnumerator();
        }

        #endregion IEnumerable

        #region XmlSerializable

        public override void ReadXml(XmlReader reader)
        {
            if (reader.HasAttributes)
            {
                while (reader.MoveToNextAttribute())
                    _sequence.Add(reader.Name, reader.Value);
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            foreach (KeyValuePair<string, string> each in _sequence)
                writer.WriteAttributeString(each.Key, each.Value);
        }

        #endregion XmlSerializable

        public XmlNameValueCollection()
        {
            _sequence = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
        }

        public void AddRange(IEnumerable<KeyValuePair<string, string>> source)
        {
            if (source is null)
                return;

            foreach (KeyValuePair<string, string> each in source)
                this.Add(each.Key, each.Value);
        }

        public bool ContainsName(string name) => _sequence.ContainsKey(name);

        public bool Exists(Predicate<KeyValuePair<string, string>> match)
        {
            if (match is null)
                throw new ArgumentNullException(nameof(match));

            return _sequence.Any(p => match(p));
        }

        public int RemoveAll(Predicate<string> match)
        {
            int result = 0;

            if (match is null)
                throw new ArgumentNullException(nameof(match));

            var buffer = _sequence.Keys
                .Where(k => match(k))
                .ToList();

            using (var enumerator = buffer.GetEnumerator())
            {
                for (; enumerator.MoveNext(); ++result)
                    _sequence.Remove(buffer[0]);
            }

            return result;
        }

        public bool TryGetValue<T>(string name, Func<string, T> converter, out T value)
        {
            return TryGetValue(name, converter, out value, default(T));
        }

        public bool TryGetValue<T>(string name, Func<string, T> converter, out T value, T defaultValue)
        {
            value = defaultValue;

            if (converter is null)
                throw new ArgumentNullException(nameof(converter));

            if (_sequence.TryGetValue(name, out string attributeValue))
            {
                value = converter(attributeValue);
                return true;
            }

            return false;
        }

        public KeyValuePair<string, string>[] ToArray() => _sequence.ToArray();

        public ICollection<string> Names => _sequence.Keys;
    };
}