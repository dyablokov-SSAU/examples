﻿using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace INFOPRO.Examples.Python.Xml
{
    public abstract class XmlSerializable : IXmlSerializable
    {
        public abstract void ReadXml(XmlReader reader);

        public abstract void WriteXml(XmlWriter writer);

        public virtual XmlSchema GetSchema() => null;
    };
}