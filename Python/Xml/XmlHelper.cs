﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace INFOPRO.Examples.Python.Xml
{
    public static class XmlHelper
    {
        #region Serialize

        public static bool SerializeToString(object obj, out string data)
        {
            return SerializeToString(obj, out data, true);
        }
        public static bool SerializeToString(object obj, out string data, bool throwOnError)
        {
            using (var stream = new MemoryStream())
            {
                var result = Serialize(obj, stream, throwOnError);
                
                using (var reader = new StreamReader(stream))
                {
                    reader.BaseStream.Position = 0;
                    data = reader.ReadToEnd();
                }

                return result;
            }
        }
        public static bool SerializeToFile(object obj, string fileName)
        {
            return SerializeToFile(obj, fileName, true);
        }
        public static bool SerializeToFile(object obj, string fileName, bool throwOnError)
        {
            using (var stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
            {
                return Serialize(obj, stream, throwOnError);
            }
        }

        public static bool Serialize(object obj, Stream stream)
        {
            return Serialize(obj, stream, true);
        }

        public static bool Serialize(object obj, Stream stream, bool throwOnError)
        {
            try
            {
                var settings = new XmlWriterSettings
                {
                    OmitXmlDeclaration = true
                };

                var serializer = new XmlSerializer(obj.GetType());

                using (var writer = XmlWriter.Create(stream, settings))
                {
                    var namespaces = new XmlSerializerNamespaces();
                    namespaces.Add(string.Empty, string.Empty);
                    serializer.Serialize(writer, obj, namespaces);
                }

                return true;
            }
            catch
            {
                if (throwOnError)
                    throw;

                return false;
            }
        }
        
        #endregion Serialize

        #region Deserialize

        public static bool DeserializeFromString<T>(string data, out T value)
        {
            return DeserializeFromString(data, out value, true);
        }
        public static bool DeserializeFromString<T>(string data, out T value, bool throwOnError)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                using (StreamWriter writer = new StreamWriter(stream) { AutoFlush = true })
                {
                    writer.Write(data);
                    stream.Position = 0;
                    return Deserialize(stream, out value, throwOnError);
                }
            }
        }
        public static bool DeserializeFromFile<T>(string fileName, out T value)
        {
            return DeserializeFromFile(fileName, out value, true);
        }
        public static bool DeserializeFromFile<T>(string fileName, out T value, bool throwOnError)
        {
            using (var stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                return Deserialize(stream, out value, throwOnError);
            }
        }
        public static bool Deserialize<T>(Stream stream, out T value)
        {
            return Deserialize(stream, out value, true);
        }
        public static bool Deserialize<T>(Stream stream, out T value, bool throwOnError)
        {
            value = default(T);
            
            try
            {
                var serializer = new XmlSerializer(typeof(T));
                value = (T)serializer.Deserialize(stream);
                return true;
            }
            catch
            {
                if (throwOnError)
                    throw;
                
                return false;
            }
        }

        #endregion Deserialize
    };
}
