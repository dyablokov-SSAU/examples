﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace INFOPRO.Examples.Python.Xml
{
    [XmlRoot(ElementName = Tag)]
    public class XmlParameterCollection : XmlSerializable, ICollection<XmlParameter>
    {
        public const string Tag = "Parameters";
        public const string ItemTag = XmlParameter.Tag;

        private List<XmlParameter> _sequence = new List<XmlParameter>();

        #region ICollection implementation

        void ICollection<XmlParameter>.CopyTo(XmlParameter[] array, int arrayIndex)
        {
            _sequence.CopyTo(array, arrayIndex);
        }

        bool ICollection<XmlParameter>.IsReadOnly => false;

        public virtual void Add(XmlParameter item) => _sequence.Add(item);

        public virtual void Clear() => _sequence.Clear();

        public virtual bool Contains(XmlParameter parameter) => _sequence.Contains(parameter);

        public virtual bool Remove(XmlParameter parameter) => _sequence.Remove(parameter);

        public int Count => _sequence.Count;

        #endregion ICollection implementation

        #region IEnumerable impelemetation

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public IEnumerator<XmlParameter> GetEnumerator() => _sequence.GetEnumerator();

        #endregion IEnumerable impelemetation

        #region XmlSerializable implementation

        public override void ReadXml(XmlReader reader)
        {
            do
            {
                switch (reader.NodeType)
                {
                    default: continue;

                    case XmlNodeType.Element:
                        {
                            if (string.Compare(reader.Name, Tag, StringComparison.InvariantCultureIgnoreCase) == 0)
                            {
                                if (reader.IsEmptyElement)
                                    return;
                            }
                            if (string.Compare(reader.Name, Tag, StringComparison.InvariantCultureIgnoreCase) != 0
                                && string.Compare(reader.Name, ItemTag, StringComparison.InvariantCultureIgnoreCase) != 0)
                                return;

                            if (string.Compare(reader.Name, ItemTag, StringComparison.InvariantCultureIgnoreCase) == 0)
                            {
                                var parameter = new XmlParameter();
                                parameter.ReadXml(reader);
                                _sequence.Add(parameter);
                            }
                        }
                        break;

                    case XmlNodeType.EndElement:
                        {
                            if (string.Compare(reader.Name, Tag, StringComparison.InvariantCultureIgnoreCase) == 0)
                                return;
                        }
                        break;
                }
            } while (reader.Read());
        }

        public override void WriteXml(XmlWriter writer)
        {
            foreach (XmlParameter parameter in _sequence)
            {
                writer.WriteStartElement(ItemTag);
                parameter.WriteXml(writer);
                writer.WriteEndElement();
            }
        }

        #endregion XmlSerializable implementation

        public XmlParameterCollection()
        {
        }

        public XmlParameterCollection(params XmlParameter[] parameters)
        {
            AddRange(parameters);
        }

        public void AddRange(IEnumerable<XmlParameter> parameters)
        {
            _sequence.AddRange(parameters ?? new XmlParameter[0]);
        }

        public bool Exists(Predicate<XmlParameter> match) => _sequence.Exists(match);

        public int RemoveAll(Predicate<XmlParameter> match) => _sequence.RemoveAll(match);

        public XmlParameter[] ToArray() => _sequence.ToArray();
    };
}