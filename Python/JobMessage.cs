﻿using INFOPRO.Examples.Python.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace INFOPRO.Examples.Python
{
    public readonly struct JobMessage
    {
        public JobMessage(JobMessageKind kind, JobMessageSource source, string text)
        {
            Kind = kind;
            Source = source;
            Text = text;
        }
        
        public JobMessageKind Kind { get; }

        public JobMessageSource Source { get; }

        public string Text { get; }
    };
}
