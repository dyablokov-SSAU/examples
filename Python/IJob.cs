﻿using INFOPRO.Examples.Python.Constants;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace INFOPRO.Examples.Python
{
    public interface IJob
    {
        Guid Id { get; }

        void CreateMessage(JobMessageKind messageType, JobMessageSource messageSource, string text);

        Task CreateMessageAsync(JobMessageKind messageType, JobMessageSource messageSource, string text);

        IDictionary<Guid, JobMessage> GetMessages();

        Task<IDictionary<Guid, JobMessage>> GetMessagesAsync();

        void CreateOrUpdateVariable(string name, string value);

        Task CreateOrUpdateVariableAsync(string name, string value);

        IEnumerable<string> GetVariableNames();

        Task<IEnumerable<string>> GetVariableNamesAsync();

        string GetVariableValue(string name);

        Task<string> GetVariableValueAsync(string name);
    };
}