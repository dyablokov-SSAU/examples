#pragma once

#define WIN32_LEAN_AND_MEAN

#include <windows.h>

typedef int APIResult;

#define APICALL __cdecl
#define NATIVEAPI extern "C" __declspec(dllexport) APIResult APICALL

constexpr auto APIRESULT_OK = 0;
constexpr auto APIRESULT_SIZE_RETRIEVED = 1;

constexpr auto APIRESULT_FAIL = -1;
constexpr auto APIRESULT_BAD_PARAMETER = -2;
constexpr auto APIRESULT_INSUFFICIENT_BUFFER = -3;
constexpr auto APIRESULT_INVALID_HANDLE = -31;

typedef int APIHandle;

NATIVEAPI CORELoadApplication(const wchar_t* pass_name, int run_mode, APIHandle* hApp);
NATIVEAPI COREExit(APIHandle hApp);
NATIVEAPI COREGetApplicationLocation(APIHandle hApp, wchar_t* buffer, int buffer_len, int* len);
NATIVEAPI COREGetApplicationName(APIHandle hApp, wchar_t* buffer, int buffer_len, int* len);
NATIVEAPI COREGetApplicationVersion(APIHandle hApp, wchar_t* buffer, int buffer_len, int* len);
NATIVEAPI COREGetApplicationEdition(APIHandle hApp, wchar_t* buffer, int buffer_len, int* len);
NATIVEAPI COREGetApplicationInfo(APIHandle hApp, wchar_t* buffer, int buffer_len, int* len);
NATIVEAPI COREGetEngineVersion(APIHandle hApp, wchar_t* buffer, int buffer_len, int* len);
NATIVEAPI COREGetSchemaSignature(APIHandle hApp, wchar_t* buffer, int buffer_len, int* len);
