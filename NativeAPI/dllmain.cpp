#include <map>
#include <string>
#include <algorithm>
#include "api.h"

template <typename K, typename V>
using storage_type = std::map<K, V>;

typedef storage_type<APIHandle, std::wstring> app_storage_type;

BOOL APIENTRY DllMain(HMODULE hModule, DWORD reason, LPVOID lpReserved)
{
    switch (reason)
    {
        case DLL_PROCESS_ATTACH:
        case DLL_THREAD_ATTACH:
        case DLL_THREAD_DETACH:
        case DLL_PROCESS_DETACH:
            break;
    }

    return TRUE;
}

app_storage_type app_storage;

std::pair<typename app_storage_type::iterator, bool> FindHandle(APIHandle hApp)
{
    auto iterator = app_storage.find(hApp);

    return std::make_pair(iterator, iterator != std::end(app_storage));
}

APIResult ReturnString(const std::wstring& src, wchar_t* buffer, int buffer_len, int* len)
{
    *len = static_cast<int>(src.length() + 1);

    if (buffer == nullptr)
        return APIRESULT_SIZE_RETRIEVED;

    if (buffer_len < static_cast<int>(src.length()) + 1)
        return APIRESULT_INSUFFICIENT_BUFFER;

    wcscpy_s(buffer, src.length() + 1, src.c_str());
    
    return APIRESULT_OK;
}

NATIVEAPI CORELoadApplication(const wchar_t* pass_name, int run_mode, APIHandle* hApp)
{
    typedef typename app_storage_type::value_type entry_type;

    auto max_entry = std::max_element(std::begin(app_storage), std::end(app_storage),
        [](const entry_type& lhs, const entry_type& rhs)->bool { return lhs.first < rhs.first; });
    
    *hApp = 1;

    if (max_entry != std::end(app_storage))
        *hApp = max_entry->first + 1;

    app_storage.insert({ *hApp,  pass_name });

    return APIRESULT_OK;
}

NATIVEAPI COREExit(APIHandle hApp)
{
    auto info = FindHandle(hApp);

    if (!info.second)
        return APIRESULT_INVALID_HANDLE;

    app_storage.erase(info.first);

    return APIRESULT_OK;
}

NATIVEAPI COREGetApplicationLocation(APIHandle hApp, wchar_t* buffer, int buffer_len, int* len)
{
    auto info = FindHandle(hApp);

    if (!info.second)
        return APIRESULT_INVALID_HANDLE;

    return ReturnString(info.first->second, buffer, buffer_len, len);
}

NATIVEAPI COREGetApplicationName(APIHandle hApp, wchar_t* buffer, int buffer_len, int* len)
{
    auto info = FindHandle(hApp);

    if (!info.second)
        return APIRESULT_INVALID_HANDLE;

    return ReturnString(L"App" + std::to_wstring(info.first->first), buffer, buffer_len, len);
}

NATIVEAPI COREGetApplicationVersion(APIHandle hApp, wchar_t* buffer, int buffer_len, int* len)
{
    auto info = FindHandle(hApp);

    if (!info.second)
        return APIRESULT_INVALID_HANDLE;

    return ReturnString(std::to_wstring(info.first->first) + L".0", buffer, buffer_len, len);
}

NATIVEAPI COREGetApplicationEdition(APIHandle hApp, wchar_t* buffer, int buffer_len, int* len)
{
    auto info = FindHandle(hApp);

    if (!info.second)
        return APIRESULT_INVALID_HANDLE;

    return ReturnString(L"TestEdition" + std::to_wstring(info.first->first), buffer, buffer_len, len);
}

NATIVEAPI COREGetApplicationInfo(APIHandle hApp, wchar_t* buffer, int buffer_len, int* len)
{
    auto info = FindHandle(hApp);

    if (!info.second)
        return APIRESULT_INVALID_HANDLE;

    return ReturnString(L"TestInfo(App" + std::to_wstring(info.first->first) + L")", buffer, buffer_len, len);
}

NATIVEAPI COREGetEngineVersion(APIHandle hApp, wchar_t* buffer, int buffer_len, int* len)
{
    auto info = FindHandle(hApp);

    if (!info.second)
        return APIRESULT_INVALID_HANDLE;

    return ReturnString(std::to_wstring(info.first->first) + L".0", buffer, buffer_len, len);
}

NATIVEAPI COREGetSchemaSignature(APIHandle hApp, wchar_t* buffer, int buffer_len, int* len)
{
    auto info = FindHandle(hApp);

    if (!info.second)
        return APIRESULT_INVALID_HANDLE;

    return ReturnString(L"App" + std::to_wstring(info.first->first) + L"Signature", buffer, buffer_len, len);
}